#!/bin/bash

set -e

echo "Checking if Docker Compose services are running..."
if docker-compose ps -q; then
    echo "Stopping and removing current Docker Compose services..."
    docker-compose down

    echo "Cleaning up unused Docker resources..."
    # Note: Be cautious with system prune as it removes all unused containers, networks, and images not just for this project
    docker system prune -f

    echo "Removed build cache and stopped services."
else
    echo "No Docker Compose services are currently running."
fi

echo "Starting services with Docker Compose..."
docker-compose up -d

echo "Services are starting in the background."
