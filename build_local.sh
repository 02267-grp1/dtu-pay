#!/bin/bash
set -e
# Define the submodule paths and URLs
submodules=(
   "message-utility"
   "dtu-pay-facade"
   "dtu-pay-money-transfer"
   "user-management"
   "token-management"
)

# Build Projects
for submodule in "${submodules[@]}"; do
    path=$(echo "$submodule" | cut -d ' ' -f 1)
    pushd "$path"
    if [ -f "build.sh" ]; then
        chmod +x ./build.sh
        ./build.sh
    fi
    popd
done


# Docker Compose Setup
./build.sh

# Ensures all microservices are up and running
sleep 60

pushd dtu-pay-end2end-tester
mvn test
