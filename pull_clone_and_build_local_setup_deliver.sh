#!/bin/bash
set -e
git submodule update --init --recursive
chmod +x ./build.sh
# Define the submodule paths and URLs
submodules=(
   "message-utility https://gitlab.gbar.dtu.dk/02267-grp1/message-utility.git"
   "dtu-pay-facade https://gitlab.gbar.dtu.dk/02267-grp1/dtu-pay-facade.git"
   "dtu-pay-money-transfer https://gitlab.gbar.dtu.dk/02267-grp1/dtu-pay-money-transfer.git"
   "user-management https://gitlab.gbar.dtu.dk/02267-grp1/user-management.git"
   "token-management https://gitlab.gbar.dtu.dk/02267-grp1/token-management.git"
)

# Pull Repositories
for submodule in "${submodules[@]}"; do
    path=$(echo "$submodule" | cut -d ' ' -f 1)
    pushd "$path"
    git pull origin main
    popd
done

# Clone Repositories
for submodule in "${submodules[@]}"; do
    path=$(echo "$submodule" | cut -d ' ' -f 1)
    if [[ ! -d "$path" ]]; then
        url=$(echo "$submodule" | cut -d ' ' -f 2)
        git clone "$url" "$path"
    fi
done

pushd dtu-pay-end2end-tester
git pull origin main

